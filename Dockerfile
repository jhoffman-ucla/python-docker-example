FROM python:3.11

RUN mkdir app
WORKDIR app
COPY requirements.txt 

RUN python install -r requirements.txt

COPY start.sh
RUN chmod +x start.sh
ENTRYPOINT start.sh