# python-docker-example

A very simple example of containerizing a python script with docker.

This is a very limited example of how to build a container, install python dependencies using requirements.txt (not strictly required, but good practice and easier than doing it manually), and run the container.

## Prerequisites

You'll need to make sure that you have [Docker desktop](https://www.docker.com/products/docker-desktop/) installed and an internet connection.

## Run the example

### Start the container

From the command line:

```
docker build . -t python-docker-example
docker run --name example-container python-docker-example
```

You should see the output of the python script after the second command.

**What did we just do?**

The first command builds a *container image* from Dockerfile.  You can think of the Dockerfile like a blueprint for your application.  `docker build` takes your blueprint and converts it into a runnable application/environment combo.  BUT, we still don't have an actual instance of our application (container) yet. 

The `docker run` command starts an actual *container* (named "example-container") from our container image.  In fact, we could start one, two, ten, a hundred, (etc.) instances of our container if desired, all of the same image.  This approach can be used to scale resources if we need it. In the context of a "compute job" we might write our application in such a way that a container starts, runs the compute job, pushes its results to a remote server, then dies.

That being said, unless you start it with the `--rm` option (delete the container as soon as it exits), the container will stop, but doesn't get deleted.  A full discussion of why this is the default behavior is beyond the scope of this README, but this is why we need to...

### Clean up after ourselves:

```
docker container stop example-container
docker container rm example-container
docker image rm python-docker-example
```

**What did we just do?**

We first stop the container (first command).  If we don't, we'll get an error when we try to delete the container.

The second command deletes the container. 

The third command deletes the image that we built.

We're now back to where we started.

Pro tip: While debugging a dockerfile, I generally run containers with the `-it --rm` options.  This allows me to use Ctrl-C to stop and delete the container when I'm ready to change something.  For our example:

```
docker run -it --rm --name example-container python-docker-example
```

## Docker

Docker in a containerization application, for both building and running containers.  Containers are kind of like virtual machines, but utilize the host system's kernel, rather than having its own whole kernel running (don't worry about it too much if that doesn't make a lot of sense.)  The key takeaway is that it makes containers an efficient, lightweight way to bundle an applications and its dependencies (i.e. the *environment*) for running in the cloud, other computers, etc.

## What problem does Docker solve?

As you develop software, you will have systems and subsystems that your software depends on.  If you write python code, as many of us do, you depend on the python *runtime* to be present on a system for your script to run; if you use numpy, your script now depends on *numpy* being present.  So if you copy that script you just wrote to a "fresh" computer, you'll get errors (first, python probably not found, then numpy not being found).  But, when it comes to sharing your code or deploying it into the cloud, this is highly inconvenient to have to install all of this from scratch.  Furthermore, if you didn't bother to pay attention to which version of your dependencies you installed, you might install everything and *still* not have your code run.

Although solutions like `requirements.txt` can help us with python dependencies specifically, what about if you also need a compiled library, or even another application to run? How can we automate the installation of that software?  All of this is what Docker can help us solve. (Of course there are other tools like Ansible that can help us, but that's for another time)

## Why use a container?

The alternatives to running containers are mainly spinning up virtual private serves (e.g., EC2, Linode).  Although pretty cheap these days, containers are usually cheaper since they utilize fewer system resources and you can run many containers on a single machine.  There are also dedicated container management cloud services (such as Amazon ECS), and containers also open up the possibility of using tools like Kubernetes that can provide sopisticated container/application management such as autoscaling based on load, automated rollout of updates, redundancy, etc.

## 